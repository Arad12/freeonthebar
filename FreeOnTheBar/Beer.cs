﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class Beer : Drink
    {
        public override string Prepare
        {
            get
            {
                return "Well, all you need to do is pour it into the glass and serve";
            }
        }
    }
}
