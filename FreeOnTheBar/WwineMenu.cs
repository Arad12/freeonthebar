﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class WhiteWineMenu : Menu
    {
        public  Dictionary<int, WhiteWine> WhiteWine_Menu { get; private set; }

        public WhiteWineMenu()
        {
            KindOfDrink = "White Wine";  
            var Carmel = new WhiteWine()
            {
                Name = "Carmel amrald rizling",
                Year = 1986
            };
            var Golan = new WhiteWine()
            {
                Name = "Golan smadar",
                Year = 1989
            };
            var Yarden = new WhiteWine()
            {
                Name = "Yarden shardone",
                Year = 1997
            };
            this.WhiteWine_Menu = new Dictionary<int, WhiteWine>();
            this.WhiteWine_Menu.Add(1, Carmel);
            this.WhiteWine_Menu.Add(2, Golan);
            this.WhiteWine_Menu.Add(3, Yarden);
        }



        public void AddWineToTheMenue(string Name, int Year)
        {
            var Wine = new WhiteWine();
            Wine.Name = Name;
            Wine.Year = Year;
            this.WhiteWine_Menu.Add(this.WhiteWine_Menu.Count + 1,Wine);
        }
    }
}
