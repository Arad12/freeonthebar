﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class Bar
    {
        public Dictionary<int, Menu> FullBarMenu { get; private set; }
        public WhiteWineMenu WhiteWineMenu { get; private set; }
        public RedWineMenu RedWineMenu { get; private set; }
        public BeerMenu BeerMenu { get; private set; }

        public Bar()
        {
            FullBarMenu = new Dictionary<int, Menu>();
            WhiteWineMenu = new WhiteWineMenu();
            RedWineMenu = new RedWineMenu();
            BeerMenu = new BeerMenu();
            FullBarMenu.Add(1, this.WhiteWineMenu);
            FullBarMenu.Add(2, this.RedWineMenu);
            FullBarMenu.Add(3, this.BeerMenu);
        }

        public void WaiterFirstQusetion()
        {
            Console.WriteLine("What can I get you, sir? \n (0 - list options) \n (100 - Leave the bar)");
            int answer = int.Parse(Console.ReadLine());
            if (answer == 100)
            {
                ClientLeft();
            }
            else if (answer == 0)
            {
                ShowMainMenu();
            }
            else
            {
                EnterValidNumber();
                WaiterFirstQusetion();
            }
        }

        public void ClientLeft()
        {
            Console.WriteLine("The Program has ended. You left the bar.");

        }

        public void ShowMainMenu()
        {
            PrintLeaveOrListOptions();
            foreach (var Menu in this.FullBarMenu)
            {
                Console.WriteLine($"{Menu.Key} - {Menu.Value.KindOfDrink}");
            }

            int Answer = EnterAnswer();
            if (ValidAnswerZeroOrHundred(Answer))
            {
                FirstQuestionOrLeft(Answer);
            }
            else if (FullBarMenu.Keys.Contains(Answer))
            {
                FirstQuestionOrLeft(Answer);
                WhiteRedOrBeerMenu(Answer);
            }
            else
            {
                EnterValidNumber();
                ShowMainMenu();
            }
        }

        public void OpenBeerMenu()
        {
            PrintLeaveOrListOptions();
            showdict(BeerMenu.Beer_Menu);
            int Answer = EnterAnswer();
            if (ValidAnswerZeroOrHundred(Answer))
            {
                FirstQuestionOrLeft(Answer);
            }
            else if (BeerMenu.Beer_Menu.Keys.Contains(Answer))
            {
                Console.WriteLine(BeerMenu.Beer_Menu[Answer].Prepare);
                WaiterFirstQusetion();
            }
            else
            {
                EnterValidNumber();
                OpenBeerMenu();
            }
        }

        public void OpenWhiteWineMenu()
        {
            PrintLeaveOrListOptions();
            showdict(WhiteWineMenu.WhiteWine_Menu);

            int Answer = EnterAnswer();
            if (ValidAnswerZeroOrHundred(Answer))
            {
                FirstQuestionOrLeft(Answer);
            }
            else if (WhiteWineMenu.WhiteWine_Menu.Keys.Contains(Answer))
            {
                Console.WriteLine(WhiteWineMenu.WhiteWine_Menu[Answer].Prepare);
                WaiterFirstQusetion();
            }
            else
            {
                EnterValidNumber();
                OpenWhiteWineMenu();
            }
        }

        public void OpenRedWineMenu()
        {
            PrintLeaveOrListOptions();
            showdict(RedWineMenu._RedWineMenu);

            int Answer = EnterAnswer();
            if (ValidAnswerZeroOrHundred(Answer))
            {
                FirstQuestionOrLeft(Answer);
            }
            else if(RedWineMenu._RedWineMenu.Keys.Contains(Answer))
            {
                Console.WriteLine(RedWineMenu._RedWineMenu[Answer].Prepare);
                WaiterFirstQusetion();
            }
            else
            {
                EnterValidNumber();
                OpenRedWineMenu();
            }
        }



        
        private void showdict<T> (Dictionary<int, T> dict)
            where T :Drink
        {
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} - {item.Value.Name}");
            }
        }

        private void EnterValidNumber()
        {
            Console.WriteLine("Enter valid value");
        }

        private void PrintLeaveOrListOptions()
        {
            Console.WriteLine("(0 - List options) \n(100 - Leave the bar)");
        }

        private int EnterAnswer()
        {
            Console.WriteLine("Enter your answer");
            int Answer = int.Parse(Console.ReadLine());
            return Answer;
        }

        private void FirstQuestionOrLeft(int Answer)
        {
            if (Answer == 0)
            {
                WaiterFirstQusetion();
            }
            else if (Answer == 100)
            {
                ClientLeft();
            }
        }

        private void WhiteRedOrBeerMenu(int Answer)
        {
            if (Answer == 1)
            {
                OpenWhiteWineMenu();
            }
            else if (Answer == 2)
            {
                OpenRedWineMenu();
            }
            else if (Answer == 3)
            {
                OpenBeerMenu();
            }
        }

        private bool ValidAnswerZeroOrHundred(int Answer)
        {
            if (Answer == 0 || Answer == 100)
            {
                return true;
            }
            return false;

        }
    }
}
