﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class WhiteWine : Wine
    {

        public override string Prepare
        {
            get
            {
                return $"Well, {this.Name} is a cold wine, therefor it served cold. \n All you need to di is take it out from the refrigerator, pour it into a glass and serve.";
            }
        }

        public override string Name
        {
            get
            {
                return $"{base.Name} ({this.Year})";
            }
        }
    }
}
