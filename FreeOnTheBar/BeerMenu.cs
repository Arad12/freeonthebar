﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class BeerMenu : Menu
    {
        public Dictionary<int, Beer> Beer_Menu { get; private set; }

        public BeerMenu()
        {
            KindOfDrink = "Beer menu";
            Beer_Menu = new Dictionary<int, Beer>();
            var Heyniken = new Beer();
            Heyniken.Name = "Heyniken";
            var Carlsberg = new Beer();
            Carlsberg.Name = "Carlsberg";
            var Macabi = new Beer();
            Macabi.Name = "Macabi";
            var Tuburg = new Beer();
            Tuburg.Name = "Tuburg";
            Beer_Menu.Add(1, Heyniken);
            Beer_Menu.Add(2, Carlsberg);
            Beer_Menu.Add(3, Macabi);
            Beer_Menu.Add(4, Tuburg);
        }

        public void AddBeerToTheMenue(string Name)
        {
            var Beer = new Beer();
            Beer.Name = Name;
            this.Beer_Menu.Add(this.Beer_Menu.Count + 1, Beer);
        }

    }
}
