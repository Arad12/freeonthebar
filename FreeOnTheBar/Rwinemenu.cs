﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class RedWineMenu : Menu
    {

        public Dictionary<int, RedWine> _RedWineMenu { get; private set; }


        public RedWineMenu()
        {
            KindOfDrink = "Red wines";
            var Cianty = new RedWine()
            {
                Name = "Cianty",
                Year = 1997
            };
            var YardenHermonMount = new RedWine()
            {
                Name = "Yarden Hermon mount",
                Year = 1994
            };
            var YardenCabarneSobinion = new RedWine()
            {
                Name = "Yarden Cabarne Sobinion",
                Year = 1997
            };
            var ChadeauMargot = new RedWine()
            {
                Name = "Chadeau vMargot",
                Year = 1997
            };
            this._RedWineMenu = new Dictionary<int, RedWine>();
            this._RedWineMenu.Add(1, Cianty);
            this._RedWineMenu.Add(2, YardenHermonMount);
            this._RedWineMenu.Add(3, YardenCabarneSobinion);
            this._RedWineMenu.Add(4, ChadeauMargot);
        }



        public void AddWineToTheMenue(string Name, int Year)
        {
            var wine = new RedWine();
            wine.Name = Name;
            wine.Year = Year;
            this._RedWineMenu.Add(this._RedWineMenu.Count + 1, wine);
        }
    }
}
