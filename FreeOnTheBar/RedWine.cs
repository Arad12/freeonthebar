﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public class RedWine : Wine
    {
        public override string Prepare
        {
            get
            {
                return $"Well, {this.Name} wine temperture's same as room temperture. \n All you need to do is pour it into a glass and serve.";
            }
        }
        public override string Name
        {
            get
            {
                return $"{base.Name} ({this.Year})";
            }
        }
    }
}
