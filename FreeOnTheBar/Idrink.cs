﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeOnTheBar
{
    public interface Idrink
    {
        string Name { get; set; }

        string  Prepare { get; }
    }
}
